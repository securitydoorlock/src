import I2C_LCD_driver
import time
import RPi.GPIO as GPIO
from mfrc522 import SimpleMFRC522
import sys
from pyfingerprint.pyfingerprint import PyFingerprint

#turn lock on
def motor_on(pin):
    GPIO.output(pin, GPIO.HIGH)

#turn lock off
def motor_off(pin):
    GPIO.output(pin, GPIO.LOW)

#get the finger that unlocks
def enrollfinger():
    try:
    	mylcd.lcd_clear()
    	mylcd.lcd_display_string("Place Finger.", 1)
    	while ( f.readImage() == False ):
            pass

        f.convertImage(0x01)

    	result = f.searchTemplate()
    	positionNumber = result[0]
    	mylcd.lcd_clear()
    	mylcd.lcd_display_string("Remove finger.", 1)
    	time.sleep(2)
    	mylcd.lcd_clear()
    	mylcd.lcd_display_string("Place finger", 1)
    	mylcd.lcd_display_string("again", 2)
    	while ( f.readImage() == False ):
            pass

    	f.convertImage(0x02)

    	if ( f.compareCharacteristics() == 0 ):
            raise Exception('Fingers do not match')

    	f.createTemplate()
    	positionNumber = f.storeTemplate()
    	mylcd.lcd_clear()
    	mylcd.lcd_display_string("Successful.", 1)
	return

    except Exception as e:
    	print('Operation failed!')
    	print('Exception message: ' + str(e))
    	exit(1)

#check if new fingerprint is matches the one that unlocks
def searchmatch():
    try:
	mylcd.lcd_clear()
        mylcd.lcd_display_string("Place Finger.", 1)
        while ( f.readImage() == False ):
            pass

        f.convertImage(0x01)

        result = f.searchTemplate()
        positionNumber = result[0]
	accuracyScore = result[1]

	if (positionNumber == -1):
            mylcd.lcd_clear()
            mylcd.lcd_display_string("Wrong Finger.", 1)
	    time.sleep(3)
	    goodfing = 0
	    motor_off(chanel)
	    return goodfing
	else:
	    mylcd.lcd_clear()
            mylcd.lcd_display_string("Right finger.", 1)
	    time.sleep(3)
	    goodfing = 1
	    return goodfing
	return

    except Exception as e:
        print('Operation failed!')
        print('Exception message: ' + str(e))
        exit(1)

#check if new rfid is matches the one that unlocks
def checkkey():
    mylcd.lcd_clear()
    mylcd.lcd_display_string("Provide key.", 1)
    attempt, text = reader.read()
    mylcd.lcd_display_string("keyd.", 2)
    if (attempt == key):
        mylcd.lcd_clear()
        mylcd.lcd_display_string("Successful", 1)
 	goodkey = 1
        time.sleep(3)
	return goodkey
    else:
        mylcd.lcd_clear()
        mylcd.lcd_display_string("Wrong key.", 1)
        goodkey = 0
        motor_off(chanel)
        time.sleep(3)
	return goodkey
    return

if __name__ == '__main__':

    chanel = 4
#if both rfid and key are right, then door unlocks
    goodkey = 0
    goodfing = 0


    GPIO.setmode(GPIO.BCM)
    GPIO.setup(chanel, GPIO.OUT)
    mylcd = I2C_LCD_driver.lcd()

    sys.path.append('/home/pi/MFRC522-python')

    reader = SimpleMFRC522()
    try:
        f = PyFingerprint('/dev/ttyUSB0', 57600, 0xFFFFFFFF, 0x00000000)
        if ( f.verifyPassword() == False ):
            raise ValueError('The given fingerprint sensor password is wrong')
    except Exception as e:
        print('The fingerprint sensor could not be initialized!')
        print('Exception message: ' + str(e))
        exit(1)

    try:
	motor_off(chanel)
	enrollfinger()
	mylcd.lcd_clear()
	mylcd.lcd_display_string("Enroll key.", 1)
        key, text = reader.read()
	mylcd.lcd_clear()
	mylcd.lcd_display_string("Key enrolled.", 1)
        time.sleep(3)
	while True:
	    goodfing = searchmatch()
	    goodkey = checkkey()
            if (goodkey == 1 and goodfing == 1):
		motor_on(chanel)
	    else:
		motor_off(chanel)
    except KeyboardInterrupt:
        GPIO.cleanup()
        pass